﻿using System;
using FakeItEasy;
using FluentAssertions;
using NUnit.Framework;

namespace WindowsStoreTest
{
    [TestFixture]
    public class RepositoryTest
    {
        [Test]
        public void TestFooFaked()
        {
            // Arrange
            var service = A.Fake<IService>();
            var serviceFoo = A.CallTo(() => service.Foo());

            serviceFoo.Returns("FooBar!");

            var subject = new Repository(service);

            // Act
            var result = subject.ServiceFoo();

            // Assert
            Assert.AreSame("FooBar!", result);
            serviceFoo.MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        public void TestFooNoFake()
        {
            // Arrange
            var service = new Service();
            var subject = new Repository(service);

            // Act
            var result = subject.ServiceFoo();

            // Assert
            Assert.AreSame("Foo!", result);
        }

        [Test]
        public void TestThrowException()
        {
            // Arrange
            var subject = new Repository(new Service());

            // Act
            Action action = subject.ThrowsException;

            // Assert
            action.ShouldThrow<ArgumentException>().WithMessage("FooFoo!");
        }

        [Test]
        public void TestExceptionsInAsyncMethodsCrashFluentAssertionsInWinRT()
        {
            // Arrange
            var subject = new Repository();

            // Act
            Action action = async () => { await subject.ExceptionsInAsyncMethodsCrashFluentAssertionsInWinRT(string.Empty); };

            // Assert
            action.ShouldThrow<ArgumentException>("Expected FluentAssertions to Crash :P");
            //.WithMessage("foo can't be null or empty\r\nParameter name: foo");
        }
    }
}
