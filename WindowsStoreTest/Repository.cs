﻿using System;
using System.Threading.Tasks;

namespace WindowsStoreTest
{
    public class Repository
    {
        public IService Service { get; set; }

        public Repository()
        {
        }

        public Repository(IService service)
        {
            Service = service;
        }

        public string ServiceFoo()
        {
            return Service.Foo();
        }

        public void ThrowsException()
        {
            throw new ArgumentException("FooFoo!", string.Empty);
        }

        public async Task<Whatever> ExceptionsInAsyncMethodsCrashFluentAssertionsInWinRT(string foo)
        {
            if (string.IsNullOrEmpty(foo))
                throw new ArgumentException("foo can't be null or empty", "foo");

            return await Task.Run(() => new Whatever());
        }
    }

    public class Whatever
    {
    }
}
